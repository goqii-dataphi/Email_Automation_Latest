package com.websystique.spring.scheduling;

/**
 * Created by Jayalakshmi on 04-09-2015.
 */

import com.websystique.spring.email.EmailAttachmentSender;
import com.websystique.spring.excel.graphGeneration;
import com.websystique.spring.jdbc.dbDAOImpl;
import com.websystique.spring.excel.excelGeneration;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

@Component("schedulerBean")
public class SchedulerBean {
	public void runScheduler(){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			System.out.println("starting");
			input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/excel.properties");
			prop.load(input);

			// function to execute the stored procedures.
			dbDAOImpl.executeProcs();

			excelGeneration.generate(dbDAOImpl.fetch(),dbDAOImpl.fetchForGraph(),prop.getProperty("excelPath"));

			//Generated excel as attachment
			String[] attachFiles = new String[1];
			attachFiles[0] = prop.getProperty("excelPath")+"DataExcel.xls";

			String filePath = prop.getProperty("excelPath")+"DataExcel.xls";

			input = null;
            input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/email.properties");
			prop.load(input);

            // Email sending With attachment
			EmailAttachmentSender.sendEmailWithAttachments(prop.getProperty("host"), prop.getProperty("port"),
					prop.getProperty("mailFrom"), prop.getProperty("password"),
					prop.getProperty("mailTo"), prop.getProperty("subject"),
					prop.getProperty("message"), attachFiles);

			System.out.println("Email sent.");

			// After mail is sent delete the file from disk
			File file = new File(filePath);
	        file.delete();
			System.out.println("File deleted");

		} catch (Exception ex) {
			System.out.println("Could not send email.");
			ex.printStackTrace();
		}

	}
}
