package com.websystique.spring.excel;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.awt.*;
import java.io.*;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.*;
import java.util.List;

/**
 * Created by Jayalakshmi on 08-09-2015.
 */
public class excelGeneration {

    private static PieDataset createPieDataset(List<Map<String,Object>> dbData) {
        // function which converts the data from the db to dataset
        DefaultPieDataset dataset = new DefaultPieDataset();
        for (Map<String, Object> d : dbData) {
            String label = "";
            Double value = Double.valueOf(0);
            int i =0;
            for (String k : d.keySet()) {
                Object obj = d.get(k);
                if (obj instanceof String)
                    label = (String)obj;
                else if (obj instanceof BigInteger)
                    value = ((BigInteger)obj).doubleValue();
                else if (obj instanceof Integer)
                    value = (double)((Integer)obj);
            }

            dataset.setValue(label, new Double(value));
        }
        return dataset;
    }

    private static DefaultCategoryDataset createBarChartDataset(List<Map<String,Object>> dbData) {
        // function which converts the data from the db to dataset
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Map<String, Object> d : dbData) {
            String label = "";
            Double value = Double.valueOf(0);
            for (String k : d.keySet()) {
                Object obj = d.get(k);
                if (obj instanceof String)
                    label = (String)obj;
                else if (obj instanceof BigInteger)
                    value = ((BigInteger)obj).doubleValue();
                else if (obj instanceof Integer)
                    value = (double)((Integer)obj);
            }
            dataset.setValue(new Double(value),"",label);
        }
        return dataset;
    }

    private static void writeExcelHeader(String colHeader,HSSFSheet sheet){
        //  Code to form the column names in the excel
        String[] header = colHeader.split(",");
        int rowNum = 0, cellNum = 0;
        Row headerRow = sheet.createRow(rowNum++);
        for (String colmnName : header) {
            Cell cell = headerRow.createCell(cellNum++);
            cell.setCellValue(colmnName);
        }
    }

    private static int writeExcelCells(HSSFSheet sheet, List<Map<String,Object>> dbData,int rowNum){
        // function to write the Data to the excel cells based on the data fetched from the db stored in "dbData"
        // Data of type String ,BigInteger and  Integer has been handled
        int cellNum = 0;
        for (Map<String, Object> d : dbData) {
            Row row = sheet.createRow(rowNum++);
            cellNum = 0;
            for (String k : d.keySet()) {
                Object obj = d.get(k);
                Cell cell = row.createCell(cellNum++);
                if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
                else if (obj instanceof BigInteger){
                    BigInteger value = (BigInteger)obj;
                    cell.setCellValue(value.doubleValue());
                }
            }
        }
        return cellNum;
    }
    private static void barChartGen(List<Map<String,Object>> dbData,String graphTitle, int picture_id,ClientAnchor my_anchor,HSSFWorkbook workbook,
                                    int colNum,int rowNum, HSSFPatriarch drawing,String graphXAxis,String graphYAxis){
        try {
            //Create a logical chart object with the chart data collected
            JFreeChart BarChartObject=ChartFactory.createBarChart(
                    graphTitle,
                    graphXAxis,
                    graphYAxis,
                    createBarChartDataset(dbData),
                    PlotOrientation.VERTICAL,
                    false,
                    true,
                    false);

            // creating custom themes
            StandardChartTheme theme = (StandardChartTheme)org.jfree.chart.StandardChartTheme.createJFreeTheme();

            theme.setTitlePaint( Color.decode( "#4572a7" ) );
            theme.setRangeGridlinePaint( Color.decode("#C0C0C0"));
            theme.setPlotBackgroundPaint( Color.white );
            theme.setChartBackgroundPaint( Color.white );
            theme.setGridBandPaint( Color.BLUE );
            theme.setBarPainter(new StandardBarPainter());
            theme.setAxisLabelPaint( Color.decode("#666666")  );

            theme.apply(BarChartObject);

            BarChartObject.getCategoryPlot().getRenderer().setSeriesPaint( 0, Color.LIGHT_GRAY);
            BarChartObject.getCategoryPlot().getRenderer().setSeriesItemLabelGenerator(0,
                    new StandardCategoryItemLabelGenerator("{2}", NumberFormat.getInstance()));
            BarChartObject.getCategoryPlot().getRenderer().setSeriesItemLabelsVisible(0, true);

            ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(chart_out,BarChartObject,500,500);

            // We can now read the byte data from output stream and stamp the chart to Excel worksheet
            picture_id = workbook.addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);

            // we close the output stream as we don't need this anymore
            chart_out.close();

            //Define top left corner, and we can resize picture suitable from there
            my_anchor.setCol1(colNum);
            my_anchor.setRow1(rowNum);

            // Invoke createPicture and pass the anchor point and ID
            HSSFPicture my_picture = drawing.createPicture(my_anchor, picture_id);

            // Call resize method, which resizes the image
            my_picture.resize();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private static void pieChartGen(List<Map<String,Object>> dbData,String graphTitle,int picture_id,ClientAnchor my_anchor,HSSFWorkbook workbook,int colNum,int rowNum, HSSFPatriarch drawing){
        try {

            JFreeChart pieChartObject = ChartFactory.createPieChart(
                    graphTitle,  // chart title
                    createPieDataset(dbData),             // data
                    true,               // include legend
                    true,               //  include tool tip
                    false
            );
            PiePlot ColorConfigurator = (PiePlot) pieChartObject.getPlot(); /* get PiePlot object for changing */
            ColorConfigurator.setBackgroundPaint(Color.WHITE); // Setting the background color to white
            ColorConfigurator.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}:{1},{2}")); // for generating the labels with the data and percentage

            ByteArrayOutputStream pie_chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(pie_chart_out, pieChartObject, 500, 400);
            // attach the picture to the work book.
            picture_id = workbook.addPicture(pie_chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);
            pie_chart_out.close();

            //Define top left corner, and we can resize picture suitable from there
            my_anchor.setCol1(colNum);
            my_anchor.setRow1(rowNum);

            // /Invoke createPicture and pass the anchor point and ID
            HSSFPicture my_picture = drawing.createPicture(my_anchor, picture_id);
            // Call resize method, which resizes the image
            my_picture.resize();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void generate(Map<String,List<Map<String,Object>>> dbSheetData,
                                Map<String,List<Map<String,Object>>> dbGraphData,String filePath){
        Properties prop = new Properties();
        Properties titleProp = new Properties();
        Properties xAxisProp = new Properties();
        Properties yAxisProp = new Properties();

        InputStream input = null;

        try{
            // Reads the header columns names
            input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/excelCol.properties");
            prop.load(input);

            // Reads the graph title from properties file
            input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/graphTitle.properties");
            titleProp.load(input);

            // Reads the graph x-Axis  from properties file
            input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/graphXAxis.properties");
            xAxisProp.load(input);

            // Reads the graph y-Axis  from properties file
            input = new FileInputStream(System.getProperty("user.dir")+"/target/classes/graphYAxis.properties");
            yAxisProp.load(input);

            System.out.println("Excel Generation");
            HSSFWorkbook workbook = new HSSFWorkbook();
            //Create a blank sheet
            for(String key:dbSheetData.keySet()) {
                int rowNum =1;
                //Based on the number of queries executed , sheets will be created
                HSSFSheet sheet = workbook.createSheet(key);
                List<Map<String,Object>> dbData = dbSheetData.get(key); // getting the List of  db data from the Map

                String colHeader = prop.getProperty(key);
                writeExcelHeader(colHeader, sheet);
                writeExcelCells(sheet,dbData,rowNum);
            }

            // graph generation
            for(String key:dbGraphData.keySet()) {
                List<Map<String,Object>> dbData = dbGraphData.get(key);
                // Create the sheet
                HSSFSheet sheet = workbook.createSheet(key);
                //  Code to form the column names
                String colHeader = prop.getProperty(key);
                writeExcelHeader(colHeader,sheet);

                int picture_id = 0; // id for the graph picture
                int rowNum = 1;
                int cellNum = 0;
                int colNum = 1;

                /* Create the drawing container */
                HSSFPatriarch drawing = sheet.createDrawingPatriarch();
                /* Create an anchor point */
                ClientAnchor my_anchor = new HSSFClientAnchor();

                colNum = writeExcelCells(sheet,dbData,rowNum);

                rowNum = rowNum+5;
                colNum = colNum++;

                String graphTitle = titleProp.getProperty(key);
                String graphYAxis = yAxisProp.getProperty(key);
                String graphXAxis = xAxisProp.getProperty(key);

                if(key.contains("Engagement_Users")){
                    // Function to generate pie chart
                    pieChartGen(dbData,graphTitle,picture_id,my_anchor,workbook,colNum,rowNum,drawing);
                }else{
                    // function to generate bar chart
                    barChartGen(dbData,graphTitle,picture_id,my_anchor,workbook,colNum,rowNum,drawing,graphXAxis,graphYAxis);
                }
            }

            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(filePath+"DataExcel.xls"));
            workbook.write(out);
            out.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}