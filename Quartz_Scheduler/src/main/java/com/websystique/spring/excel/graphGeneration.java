package com.websystique.spring.excel;

/**
 * Created by suresh on 14-09-2015.
 */
import java.io.*;

import java.awt.Color;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.labels.*;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.renderer.category.StandardBarPainter;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;


import java.text.NumberFormat;

public class graphGeneration {

    // Sample Graph Generation Code. with hardcoded data for reference

    /*private static PieDataset createPieDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("High", new Double(2223));
        dataset.setValue("Low", new Double(1415));
        dataset.setValue("Medium", new Double(1138));
        dataset.setValue("Passive", new Double(2297));
        return dataset;
    }


    public static void generateBarChart(String filePath){
        try{

            DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();


            HSSFWorkbook  my_workbook = new HSSFWorkbook ();

            my_bar_chart_dataset.addValue((double)4,"Users"," ");
            my_bar_chart_dataset.addValue((double)10,"Users","Australia");
            my_bar_chart_dataset.addValue((double)1,"Users","HongKong");
            my_bar_chart_dataset.addValue((double)143,"Users","India");
            my_bar_chart_dataset.addValue((double)2,"Users","SouthAfrica");
            my_bar_chart_dataset.addValue((double)1,"Users","UnitedKingdom");
            my_bar_chart_dataset.addValue((double)3,"Users","UnitedStates");


            JFreeChart pieChartObject=ChartFactory.createPieChart(
                    "Active Users Breakup",  // chart title
                    createPieDataset(),             // data
                    true,               // include legend
                    true,               //  include tool tip
                    false
            );

            PiePlot ColorConfigurator = (PiePlot) pieChartObject.getPlot(); *//* get PiePlot object for changing *//*
            ColorConfigurator.setBackgroundPaint(Color.WHITE);
            ColorConfigurator.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}:{1},{2}"));

            ByteArrayOutputStream pie_chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(pie_chart_out,pieChartObject,500,400);
            int pie_picture_id = my_workbook.addPicture(pie_chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);
                *//* we close the output stream as we don't need this anymore *//*
            pie_chart_out.close();

            *//* Create a logical chart object with the chart data collected *//*
            JFreeChart BarChartObject=ChartFactory.createBarChart(
                    "# Churns Past Week country wise",
                    "Country",
                    "Users",
                    my_bar_chart_dataset,
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false);

            StandardChartTheme theme = (StandardChartTheme)org.jfree.chart.StandardChartTheme.createJFreeTheme();

            theme.setTitlePaint( Color.decode( "#4572a7" ) );
            theme.setRangeGridlinePaint( Color.decode("#C0C0C0"));
            theme.setPlotBackgroundPaint( Color.white );
            theme.setChartBackgroundPaint( Color.white );
            theme.setGridBandPaint( Color.BLUE );
            theme.setBarPainter(new StandardBarPainter());
            theme.setAxisLabelPaint( Color.decode("#666666")  );

            theme.apply(BarChartObject);

            BarChartObject.getCategoryPlot().getRenderer().setSeriesPaint( 0, Color.BLUE);
            BarChartObject.getCategoryPlot().getRenderer().setSeriesItemLabelGenerator(0,
                    new StandardCategoryItemLabelGenerator("{2}",NumberFormat.getInstance()));
            BarChartObject.getCategoryPlot().getRenderer().setSeriesItemLabelsVisible(0, true);

                *//* Dimensions of the bar chart *//*
            int width=500; *//* Width of the chart *//*
            int height=400; *//* Height of the chart *//*

            *//* We don't want to create an intermediate file. So, we create a byte array output stream
               and byte array input stream
               And we pass the chart data directly to input stream through this *//*
            *//* Write chart as PNG to Output Stream *//*
            ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(chart_out,BarChartObject,width,height);
                *//* We can now read the byte data from output stream and stamp the chart to Excel worksheet *//*
            int my_picture_id = my_workbook.addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);
                *//* we close the output stream as we don't need this anymore *//*
            chart_out.close();

            HSSFSheet my_sheet = my_workbook.createSheet();
                *//* Create the drawing container *//*
            HSSFPatriarch drawing = my_sheet.createDrawingPatriarch();
                *//* Create an anchor point *//*
            ClientAnchor my_anchor = new HSSFClientAnchor();
                *//* Define top left corner, and we can resize picture suitable from there *//*
            my_anchor.setCol1(1);
            my_anchor.setRow1(1);
                *//* Invoke createPicture and pass the anchor point and ID *//*
            HSSFPicture  my_picture = drawing.createPicture(my_anchor, my_picture_id);
                *//* Call resize method, which resizes the image *//*
            my_picture.resize();

            *//*Pie Chart*//*
            ClientAnchor pie_anchor = new HSSFClientAnchor();
                *//* Define top left corner, and we can resize picture suitable from there *//*
            pie_anchor.setCol1(13);
            pie_anchor.setRow1(1);

            HSSFPicture  pie_picture = drawing.createPicture(pie_anchor, pie_picture_id);
                *//* Call resize method, which resizes the image *//*
            pie_picture.resize();

                *//* Write changes to the workbook *//*
            FileOutputStream out = new FileOutputStream(new File(filePath+"test_graph_chart.xls"));
            my_workbook.write(out);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/
}