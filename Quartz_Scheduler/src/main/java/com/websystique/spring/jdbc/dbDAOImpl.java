package com.websystique.spring.jdbc;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.*;

/**
 * Created by Jayalakshmi on 07-09-2015.
 */
public class dbDAOImpl {
    private static  DataSource dataSource;
    private static JdbcTemplate jdbcTemplate;
    private static SimpleJdbcCall simpleJdbcCall;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static void executeProcs(){
        Properties prop = new Properties();
        InputStream inputStr = null;
        try {
            inputStr = new FileInputStream(System.getProperty("user.dir")+"/target/classes/procs.properties");
            prop.load(inputStr);
            for(String key : prop.stringPropertyNames()){
                String value = prop.getProperty(key);
                simpleJdbcCall = new SimpleJdbcCall(dataSource).withProcedureName(value);
                simpleJdbcCall.execute();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Map<String,List<Map<String,Object>>> fetch() {
        Properties prop = new Properties();
        InputStream inputStr = null;
        Map<String,List<Map<String,Object>>> returnMap=  new TreeMap<String,List<Map<String,Object>>>();
        try {
            inputStr = new FileInputStream(System.getProperty("user.dir")+"/target/classes/dataExcelQuery.properties");
            prop.load(inputStr);
            for(String key : prop.stringPropertyNames()) {
                String value = prop.getProperty(key);
                List<Map<String, Object>> empRows = jdbcTemplate.queryForList(value);
                returnMap.put(key,empRows);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return returnMap;
    }

    public static Map<String,List<Map<String,Object>>> fetchForGraph() {
        Properties prop = new Properties();
        InputStream inputStr = null;
        Map<String,List<Map<String,Object>>> returnMap=  new TreeMap<String,List<Map<String,Object>>>();
        try {
            inputStr = new FileInputStream(System.getProperty("user.dir")+"/target/classes/graphQuery.properties");
            prop.load(inputStr);
            for(String key : prop.stringPropertyNames()) {
                String value = prop.getProperty(key);

                List<Map<String, Object>> empRows = jdbcTemplate.queryForList(value);

                returnMap.put(key,empRows);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return returnMap;
    }
}