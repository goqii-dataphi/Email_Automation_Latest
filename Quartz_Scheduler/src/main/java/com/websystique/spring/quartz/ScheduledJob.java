package com.websystique.spring.quartz;

/**
 * Created by Jayalakshmi on 04-09-2015.
 */

import com.websystique.spring.email.EmailAttachmentSender;
import com.websystique.spring.jdbc.dbDAOImpl;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.websystique.spring.scheduling.SchedulerBean;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ScheduledJob extends QuartzJobBean{

	
	private SchedulerBean schedulerBean;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
        schedulerBean.runScheduler();
	}

	public void setSchedulerBean(SchedulerBean schedulerBean) {
		this.schedulerBean = schedulerBean;
	}
}
